## Vagrant and Ansible Setup

This setup contains all config files to experiment with several services like salt, icinga, jenkins etc.

* **Vagrant** is used to provision virtual machines
* **Ansible** is used to deploy Salt on the virtual machines. This is the sole purpose of ansible in this project
* **Salt** is used to set up the examples.


### Vagrant

* **Requirements**: vagrant and VirtualBox must be installed

A vagrant configuration in `vagrant/Vagrantfile` can be used to start a custom number of nodes. The Vagrantfile copies the id_rsa.pub in the `vagrant/` directory to the authorized_keys file of `vagrant@node1`, so replace it with your own.

```shell
$ cd vagrant
$ vagrant up
```

### Ansible

* **Requirements**: ansible must be installed

The inventory in `ansible/inventory` contains 3 vagrant nodes with their private ip addresses. Change this file to use more/fewer nodes. 

A base playbook `ansible/playbooks/upgrade_and_base_packages.yml` upgrades the nodes and installes several base packages like `curl` and `gpg`. All further playbooks work standalone and are examples for a specific service.

```shell
$ cd ansible
$ ansible-playbook playbooks/upgrade_and_base_packages.yml
$ ansible-playbook playbooks/salt.yml
```

### Salt

After installing the playbook, a salt setup is ready to go. Use the `salt` command on `vagrant ssh node1` to control salt.

The salt configurations are available in the `salt` directory. Use the `run.sh` script to copy them to node1 and `salt state.highstate` them.

* icinga: sets up icinga on node2 via salt. See icinga/salt/icinga/files for configuration files

#### Links

* [Salt Module Index](https://docs.saltproject.io/en/latest/py-modindex.html)
* [Install Salt on Debian](https://docs.saltproject.io/salt/install-guide/en/latest/topics/install-by-operating-system/debian.html)
* [Configure Master and Minions](https://docs.saltproject.io/salt/install-guide/en/latest/topics/configure-master-minion.html#configure-master-minion)
* [Salt User Guide](https://docs.saltproject.io/salt/user-guide/en/latest/)
