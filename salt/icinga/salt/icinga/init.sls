icinga_repos:
  pkgrepo.managed:
    - humanname: deb-icinga
    - name: deb https://packages.icinga.com/debian icinga-bullseye main
    - file: /etc/apt/sources.list.d/deb-icinga.list
    - key_url: https://packages.icinga.com/icinga.key

icinga2:
  pkg.installed:
    - pkgs:
      - icinga2
      - icingadb-redis
      - monitoring-plugins
  service:
    - running
    - enable: True

icingadb-redis:
  service:
    - running
    - enable: True


# run the icinga api setup if the ca.crt does not exist
'icinga2 api setup':
  cmd.run:
    - creates:
      - /var/lib/icinga2/ca//ca.crt
      - /var/lib/icinga2/ca//ca.key

# enable icingadb
'icinga2 feature enable icingadb':
  cmd.run:
    - creates:
      - /etc/icinga2/features-enabled/icingadb.conf

# TODO: icingadb+mysql https://icinga.com/docs/icinga-db/latest/doc/02-Installation/03-Debian/#installing-icinga-db-package
