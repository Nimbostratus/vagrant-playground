#!/bin/sh

NODE1_IP=192.168.56.11


if [ -z "$1" ]; then
    echo "usage: run.sh directory"
    exit 1
fi

ssh root@${NODE1_IP} "rm -rf /srv"
scp -r "$1" root@${NODE1_IP}:/srv
ssh root@${NODE1_IP} "salt \* state.highstate"
